package mk.mladen.neostar.neo4j.node;

import java.io.Serializable;
import java.util.Set;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.EnumString;

@NodeEntity(label = "Request")
public class RequestNode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	private String requestNumber;

	@EnumString(value = RequestType.class)
	private RequestType requestType;

	private String timestamp;

	@Relationship(type = "CONTAINS")
	private Set<PersonNode> signingPersons;

	@Relationship(type = "CONTAINS")
	private Set<DocumentNode> scannedDocuments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Set<PersonNode> getSigningPersons() {
		return signingPersons;
	}

	public void setSigningPersons(Set<PersonNode> signingPersons) {
		this.signingPersons = signingPersons;
	}

	public Set<DocumentNode> getScannedDocuments() {
		return scannedDocuments;
	}

	public void setScannedDocuments(Set<DocumentNode> scannedDocuments) {
		this.scannedDocuments = scannedDocuments;
	}

	@Override
	public String toString() {
		return String.format("RequestNode [requestNumber=%s, requestType=%s]", requestNumber, requestType);
	}

	
}
