package mk.mladen.neostar.neo4j.node;

import java.io.Serializable;
import java.util.Set;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "Person")
public class PersonNode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	private String personNumber;

	private String name;

	private Integer age;

	private String citizenship;

	private String phone;

	private String email;

	@Relationship(type = "CONTAINS_REQUEST")
	private Set<RequestNode> requests;

	@Relationship(type = "DONATED")
	private Set<ConsentNode> consentsDonated;

	@Relationship(type = "RECEIVED")
	private Set<ConsentNode> consentsReceived;

	@Relationship(type = "CONTAINS_DOCUMENT")
	private Set<DocumentNode> documents;

	@Relationship(type = "CONTAINS_REGISTRATION")
	private Set<RegistrationNode> registrations;

	@Relationship(type = "CONTAINS_NOTIFICATION")
	private Set<NotificationNode> notifications;

	@Relationship(type = "CONTAINS_CHILD")
	private Set<PersonNode> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPersonNumber() {
		return personNumber;
	}

	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<RequestNode> getRequests() {
		return requests;
	}

	public void setRequests(Set<RequestNode> requests) {
		this.requests = requests;
	}

	public Set<ConsentNode> getConsentsDonated() {
		return consentsDonated;
	}

	public void setConsentsDonated(Set<ConsentNode> consentsDonated) {
		this.consentsDonated = consentsDonated;
	}

	public Set<ConsentNode> getConsentsReceived() {
		return consentsReceived;
	}

	public void setConsentsReceived(Set<ConsentNode> consentsReceived) {
		this.consentsReceived = consentsReceived;
	}

	public Set<DocumentNode> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<DocumentNode> documents) {
		this.documents = documents;
	}

	public Set<RegistrationNode> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(Set<RegistrationNode> registrations) {
		this.registrations = registrations;
	}

	public Set<NotificationNode> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<NotificationNode> notifications) {
		this.notifications = notifications;
	}

	public Set<PersonNode> getChildren() {
		return children;
	}

	public void setChildren(Set<PersonNode> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return String.format("PersonNode [personNumber=%s, name=%s, citizenship=%s]", personNumber, name, citizenship);
	}

}
