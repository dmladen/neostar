package mk.mladen.neostar.neo4j.node;

import java.io.Serializable;
import java.util.Set;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "Case")
public class CaseNode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	private String caseNumber;

	private String timestamp;

	@Relationship(type = "CONTAINS_PERSON")
	private Set<PersonNode> persons;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Set<PersonNode> getPersons() {
		return persons;
	}

	public void setPersons(Set<PersonNode> persons) {
		this.persons = persons;
	}

	@Override
	public String toString() {
		return String.format("CaseNode [caseNumber=%s]", caseNumber);
	}

}