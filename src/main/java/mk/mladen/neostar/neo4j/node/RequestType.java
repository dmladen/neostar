package mk.mladen.neostar.neo4j.node;

import java.util.Arrays;

public enum RequestType {

	PASSPORT_ORDINARY("passport_ordinary"), 
	PASSPORT_DIPLOMATIC("passport_diplomatic"), 
	NATIONAL_ID("national_id");

	private String code;

	RequestType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	/**
	 * Return request type by code
	 *
	 * @param code - Code to compare
	 * @return Request type
	 * @throws RuntimeException if request type is not found
	 */
	public static RequestType getValueByCode(String code) {
		return Arrays.stream(values()).filter(rt -> rt.getCode().equals(code)).findFirst()
				.orElseThrow(() -> new RuntimeException("Request type was not found for '" + code + "'"));
	}
}
