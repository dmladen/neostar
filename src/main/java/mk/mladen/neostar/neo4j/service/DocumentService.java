package mk.mladen.neostar.neo4j.service;

import mk.mladen.neostar.neo4j.node.DocumentNode;

public interface DocumentService {

	Long storeDocument(DocumentNode node);

    DocumentNode findById(Long nodeId);

    DocumentNode findById(Long nodeId, int depth);

    DocumentNode findByDocumentId(String documentId);

    DocumentNode findByCaseIdAndDocumentId(String caseId, String documentId);

    void saveDocument(DocumentNode documentNode, int depth);
}
