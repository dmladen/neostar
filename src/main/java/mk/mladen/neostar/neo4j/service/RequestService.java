package mk.mladen.neostar.neo4j.service;

import mk.mladen.neostar.neo4j.node.RequestNode;

public interface RequestService {

    Long storeRequest(String caseId, String personId, RequestNode requestNode);

    Long saveRequest(RequestNode requestNode);

    RequestNode findRequestByRequestID(String requestID);

    void delete(RequestNode requestNode);
}
