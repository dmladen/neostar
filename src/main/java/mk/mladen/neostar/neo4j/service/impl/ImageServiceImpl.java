package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.repository.ImageRepository;
import mk.mladen.neostar.neo4j.service.ImageService;

@Component
@Transactional
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageRepository imageRepository;
}
