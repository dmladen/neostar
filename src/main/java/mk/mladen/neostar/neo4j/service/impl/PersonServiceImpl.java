package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.PersonNode;
import mk.mladen.neostar.neo4j.repository.PersonRepository;
import mk.mladen.neostar.neo4j.service.PersonService;

@Component
@Transactional
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository pesonRepository;
	
	@Override
	public PersonNode findByPersonID(String personId, int depth) {
		return pesonRepository.findById(Long.valueOf(personId), depth).orElse(null);
	}

	@Override
	public PersonNode findById(Long id) {
		return pesonRepository.findById(id).orElse(null);
	}

	@Override
	public PersonNode savePerson(PersonNode personNode) {
		return pesonRepository.save(personNode);
	}

	@Override
	public PersonNode savePerson(PersonNode personNode, int depth) {
		return pesonRepository.save(personNode, depth);
	}

}
