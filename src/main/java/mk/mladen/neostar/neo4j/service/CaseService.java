package mk.mladen.neostar.neo4j.service;

import java.util.List;

import mk.mladen.neostar.neo4j.node.CaseNode;

public interface CaseService {

	CaseNode findByCaseID(String caseId, int depth);
	
	CaseNode save(CaseNode caseNode, int depth);
	
	List<String> findAllCases();
}
