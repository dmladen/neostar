package mk.mladen.neostar.neo4j.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.CaseNode;
import mk.mladen.neostar.neo4j.repository.CaseRepository;
import mk.mladen.neostar.neo4j.service.CaseService;

@Component
@Transactional
public class CaseServiceImpl implements CaseService {

	@Autowired
	private CaseRepository caseRepository;
	
	@Override
	public CaseNode findByCaseID(String caseId, int depth) {
		return caseRepository.findById(Long.valueOf(caseId)).orElse(null);
	}

	@Override
	public CaseNode save(CaseNode caseNode, int depth) {
		return caseRepository.save(caseNode);
	}

	@Override
	public List<String> findAllCases() {
		// TODO Auto-generated method stub
		return null;
	}

}
