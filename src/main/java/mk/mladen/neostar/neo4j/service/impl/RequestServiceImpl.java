package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.RequestNode;
import mk.mladen.neostar.neo4j.repository.RequestRepository;
import mk.mladen.neostar.neo4j.service.RequestService;

@Component
@Transactional
public class RequestServiceImpl implements RequestService {

	@Autowired
	private RequestRepository requestRepository;
	
	@Override
	public Long storeRequest(String caseId, String personId, RequestNode requestNode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long saveRequest(RequestNode requestNode) {
		return requestRepository.save(requestNode).getId();
	}

	@Override
	public RequestNode findRequestByRequestID(String requestNumber) {
		return requestRepository.findByRequestNumber(requestNumber);
	}

	@Override
	public void delete(RequestNode requestNode) {
		// TODO Auto-generated method stub
		
	}

}
