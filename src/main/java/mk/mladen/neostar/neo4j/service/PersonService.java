package mk.mladen.neostar.neo4j.service;

import mk.mladen.neostar.neo4j.node.PersonNode;

public interface PersonService {

    PersonNode findByPersonID(String personId, int depth);

    PersonNode findById(Long id);

    PersonNode savePerson(PersonNode personNode);

    PersonNode savePerson(PersonNode personNode, int depth);
}
