package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.repository.RegistrationRepository;
import mk.mladen.neostar.neo4j.service.RegistrationService;

@Component
@Transactional
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private RegistrationRepository registrationRepository;
}
