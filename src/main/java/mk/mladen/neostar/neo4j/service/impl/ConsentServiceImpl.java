package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.repository.ConsentRepository;
import mk.mladen.neostar.neo4j.service.ConsentService;

@Component
@Transactional
public class ConsentServiceImpl implements ConsentService {

	@Autowired
	private ConsentRepository consentRepository;
}
