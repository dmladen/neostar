package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.DocumentNode;
import mk.mladen.neostar.neo4j.repository.DocumentRepository;
import mk.mladen.neostar.neo4j.service.DocumentService;

@Component
@Transactional
public class DocumentServiceImpl implements DocumentService {
	
	@Autowired
	private DocumentRepository documentRepository;

	@Override
	public Long storeDocument(DocumentNode node) {
		return documentRepository.save(node).getId();
	}

	@Override
	public DocumentNode findById(Long nodeId) {
		return documentRepository.findById(nodeId).orElse(null);
	}

	@Override
	public DocumentNode findById(Long nodeId, int depth) {
		return documentRepository.findById(nodeId, depth).orElse(null);
	}

	@Override
	public DocumentNode findByDocumentId(String documentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentNode findByCaseIdAndDocumentId(String caseId, String documentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveDocument(DocumentNode documentNode, int depth) {
		documentRepository.save(documentNode, depth);
	}

}
