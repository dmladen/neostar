package mk.mladen.neostar.neo4j.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.repository.NotificationRepository;
import mk.mladen.neostar.neo4j.service.NotificationService;

@Component
@Transactional
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;
}
