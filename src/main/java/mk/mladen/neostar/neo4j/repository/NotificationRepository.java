package mk.mladen.neostar.neo4j.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.NotificationNode;

@Transactional
public interface NotificationRepository extends Neo4jRepository<NotificationNode, Long> {

    @Query("MATCH (n:Notification)-[*0..]->(any) WHERE ID(n)={id} DETACH DELETE n, any")
    void deleteNotificationById(@Param("id") Long id);

}
