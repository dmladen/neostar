package mk.mladen.neostar.neo4j.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.RegistrationNode;

@Transactional
public interface RegistrationRepository extends Neo4jRepository<RegistrationNode, Long> {

    @Query("MATCH (r:Registration)-[*0..]->(any) WHERE ID(r)={id} DETACH DELETE r, any")
    void deleteRegistrationtById(@Param("id") Long id);

}
