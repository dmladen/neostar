package mk.mladen.neostar.neo4j.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.CaseNode;

@Transactional
public interface CaseRepository extends Neo4jRepository<CaseNode, Long> {

	@Query("MATCH (case:Case {caseID:{caseID}}) RETURN case")
    CaseNode findByCaseID(@Param("caseID") String caseID);
	
	@Query("MATCH (case:Case)-[*0..]->(any) WHERE ID(case)={id} DETACH DELETE case, any")
    void deleteCaseById(@Param("id") Long id);
	
	@Query("MATCH (case:Case {caseID:{caseID}})-[*0..]->(any) DETACH DELETE case, any")
    void deleteByCaseID(@Param("caseID") String caseID);
}
