package mk.mladen.neostar.neo4j.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.PersonNode;

@Transactional
public interface PersonRepository extends Neo4jRepository<PersonNode, Long> {

    PersonNode findByPersonID(String personID);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person) RETURN p")
    List<PersonNode> findPersonsByCaseID(@Param("caseID") String caseID);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person {personID:{personID}}) " +
            "-[:CONTAINS_CUSTODIAN]->(custodians:PersonRelative) WITH custodians " +
            "MATCH (person:Person) WHERE ID(person)=custodians.relativeId " +
            "RETURN person")
    List<PersonNode> findCustodiansByCaseIDAndPersonID(@Param("caseID") String caseID, @Param("personID") String personID);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person {personID:{personID}})" +
            "-[:CONTAINS_CHILD]->(children:PersonRelative) WITH children " +
            "MATCH (person:Person) WHERE ID(person)=children.relativeId " +
            "RETURN person")
    List<PersonNode> findChildrenByCaseIDAndPersonID(@Param("caseID") String caseID, @Param("personID") String personID);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person) WHERE p.personID IS NOT NULL RETURN p")
    List<PersonNode> findEnrolledPersonsByCaseID(@Param("caseID") String caseID);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person {personID:{personID}}) RETURN p")
    PersonNode findByCaseIDAndPersonID(@Param("caseID") String caseID, @Param("personID") String personID);

    @Query("MATCH (p:Person)-[*0..]->(any) WHERE ID(p)={id} DETACH DELETE p, any")
    void deletePersonById(@Param("id") Long id);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person)-[:CONTAINS_DOCUMENT]->" +
            "(Document {documentId:{documentId}}) RETURN p")
    PersonNode findByCaseIDAndDocumentId(@Param("caseID") String caseID, @Param("documentId") String documentId);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person)-[:CONTAINS_REQUEST]->" +
            "(Request {requestID:{requestID}}) RETURN p")
    PersonNode findByCaseIDAndRequestID(@Param("caseID") String caseID, @Param("requestID") String requestID);

    @Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(p:Person {nin:{nin}}) RETURN p")
    PersonNode findByCaseIDAndNin(@Param("caseID") String caseID, @Param("nin") String nin);

    @Query("MATCH (p:Person), (pr:PersonRelative) WHERE ID(p)={pid} AND ID(pr)={prid} CREATE (p)-[:CONTAINS_CUSTODIAN]->(pr)")
    void linkCustodianToPerson(@Param("pid") Long pid, @Param("prid") Long prid);

    @Query("MATCH (p:Person), (pr:PersonRelative) WHERE ID(p)={pid} AND ID(pr)={prid} CREATE (p)-[:CONTAINS_CHILD]->(pr)")
    void linkChildToPerson(@Param("pid") Long pid, @Param("prid") Long prid);

}
