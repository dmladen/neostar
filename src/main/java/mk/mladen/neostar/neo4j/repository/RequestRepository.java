package mk.mladen.neostar.neo4j.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.RequestNode;
import mk.mladen.neostar.neo4j.node.RequestType;

@Transactional
public interface RequestRepository extends Neo4jRepository<RequestNode, Long> {

	RequestNode findByRequestNumber(String requestNumber);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person {personID:{personID}})-[:CONTAINS_REQUEST]->(r:Request {requestType:{requestType}}) RETURN r")
	List<RequestNode> findByCaseIDAndPersonIDAndRequestType(@Param("caseID") String caseID, @Param("personID") String personID, @Param("requestType") RequestType requestType);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person {personID:{personID}})-[:CONTAINS_REQUEST]->(r:Request) WHERE r.requestType <> {requestType} RETURN r")
	List<RequestNode> findByCaseIDAndPersonIDAndNotRequestType(@Param("caseID") String caseID, @Param("personID") String personID, @Param("requestType") RequestType requestType);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person {personID:{personID}})-[:CONTAINS_REQUEST]->(r:Request) RETURN r")
	List<RequestNode> findByCaseIDAndPersonID(@Param("caseID") String caseID, @Param("personID") String personID);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person {personID:{personID}})-[:CONTAINS_REQUEST]->(r:Request {requestType:{requestType}, consentType:{consentType}}) RETURN r LIMIT 1")
	RequestNode findByCaseIDAndPersonIDAndRequestTypeAndConsentType(@Param("caseID") String caseID, @Param("personID") String personID, @Param("requestType") RequestType requestType, @Param("consentType") String consentType);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person {personID:{personID}})-[:CONTAINS_REQUEST]->(r:Request {requestType:{requestType}, consentType:{consentType}}) RETURN r")
	List<RequestNode> findNodesByCaseIDAndPersonIDAndRequestTypeAndConsentType(@Param("caseID") String caseID, @Param("personID") String personID, @Param("requestType") RequestType requestType, @Param("consentType") String consentType);

	@Query("MATCH (r:Request)-[*0..]->(any) WHERE ID(r)={id} DETACH DELETE r, any")
	void deleteRequestById(@Param("id") Long id);

}
