package mk.mladen.neostar.neo4j.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.DocumentNode;

@Transactional
public interface DocumentRepository extends Neo4jRepository<DocumentNode, Long> {

	List<DocumentNode> findByType(String type);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person)-[:CONTAINS_DOCUMENT]-> (d:Document {documentId:{documentId}}) return d")
	DocumentNode findByCaseIDAndDocumentId(@Param("caseID") String caseID, @Param("documentId") String documentId);

	@Query("MATCH (Case {caseID:{caseID}})-[:CONTAINS_PERSON]->(Person {personID:{personID}})-[:CONTAINS_DOCUMENT]-> (d:Document {documentId:{documentId}}) return d")
	DocumentNode findByCaseIDAndPersonIDAndDocumentId(@Param("caseID") String caseID, @Param("personID") String personID, @Param("documentId") String documentId);

	@Query("MATCH (d:Document)-[*0..]->(any) WHERE ID(d)={id} DETACH DELETE d, any")
	void deleteDocumentById(@Param("id") Long id);

}
