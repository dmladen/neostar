package mk.mladen.neostar.neo4j.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import mk.mladen.neostar.neo4j.node.ConsentNode;

@Transactional
public interface ConsentRepository extends Neo4jRepository<ConsentNode, Long> {

    @Query("MATCH (c:Consent)-[*0..]->(any) WHERE ID(c)={id} DETACH DELETE c, any")
    void deleteConsentById(@Param("id") Long id);

}
